
class Parser:

    def __init__(self, builder):
        self.builder = builder

    def parse_data(self, filename):

        self.builder.add_header("Report: " + filename)

        self.builder.begin_data()

        with open(filename, 'r') as file:
            for line in file:
                self.builder.add_row(line.split())

        self.builder.end_data()

        self.builder.add_footer("Copyright 2016")

        return self.builder.get_report()


class ReportBuilder:

    def add_header(self, text):
        pass

    def begin_data(self, caption=''):
        pass

    def end_data(self):
        pass

    def add_row(self, data):
        pass

    def add_footer(self, text):
        pass

    def get_report(self):
        pass

class HtmlReportBuilder(ReportBuilder):

    def __init__(self):
        self.html = ''

    def add_header(self, text):
        self.html = ''
        self.html = '<h1>' + text + '</h1>\n'

    def begin_data(self, caption=''):
        self.html += '<table>\n'

    def end_data(self):
        self.html += ' </table>\n'

    def add_row(self, data):
        self.html += '      <tr>\n'

        for item in data:
            self.html += '        <td>'
            self.html += item
            self.html += '</td>\n'
        self.html += '      </tr>\n'

    def add_footer(self, text):
        self.html += '<div class="footer">' + text + '</div>\n'

    def get_report(self):
        return self.html


class PdfReportBuilder(ReportBuilder):

    def __init__(self):
        self.pdf_elements = []

    def add_header(self, text):
        self.pdf_elements.clear()
        self.pdf_elements.append('pdf header: ' + text)

    def begin_data(self, caption=''):
        self.pdf_elements.append('pdf table: ' + caption)

    def end_data(self):
        pass

    def add_row(self, data):
        self.pdf_elements.append('pdf row: ' + str(data))

    def add_footer(self, text):
        self.pdf_elements.append('pdf footer: ' + text)

    def get_report(self):
        return self.pdf_elements


def main():

    html_builder = HtmlReportBuilder()
    parser = Parser(html_builder)

    parser.parse_data('data.txt')
    report_html = html_builder.get_report()

    print(report_html)

    print("-" * 40)

    parser.builder = PdfReportBuilder()
    report_pdf = parser.parse_data('data.txt')
    print(report_pdf)

if __name__ == '__main__':
    main()



