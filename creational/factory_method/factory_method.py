
class ServiceA:

    def __init__(self, address):
        self.name = "ServiceA"
        self.address = address

    def run(self):
        print("{} is running on {}".format(self.name, self.address))


class ServiceB(ServiceA):

    def __init__(self, address):
        super().__init__(address)
        self.name = "ServiceB"
        self.counter = 0

    def run(self):
        self.counter += 1
        print("{} is running on {}".format(self.name, self.address))


class ServiceC:

    def __init__(self, address):
        self.address = address
        self.name = "ServiceC"

    def run(self):
        print("{} is running on {}".format(self.name, self.address))


class Client:

    def __init__(self, factory):
        self.factory = factory

    def do_stuff(self):
        #...
        srv = self.factory("192.168.1.2")
        srv.run()

def get_service(address):
    srv =  ServiceC(address)
    #...
    return srv


class ServiceFactory:
    def __init__(self):
        self.creators = {}

    def register_factory(self, id, creator):
        self.creators[id] = creator

    def create(self, id, address):
        return self.creators[id](address)

    def __getitem__(self, item):
        return self.creators[item]


def main():
    client = Client(ServiceA)
    client.do_stuff()

    client.factory = ServiceB
    client.do_stuff()

    client.factory = get_service
    client.do_stuff()

    print("*" * 40)

    factory = ServiceFactory()
    factory.register_factory("A", ServiceA)
    factory.register_factory("B", lambda address: ServiceB(address))
    factory.register_factory("C", get_service)

    addresses = ["127.0.0.1", "192.168.1.2", "192.168.2.2"]
    ids = "ABC"

    services = [factory.create(id, address) for id, address in zip(ids, addresses)]

    for srv in services:
        srv.run()

if __name__ == '__main__':
    main()

