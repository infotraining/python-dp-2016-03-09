
instance_ = None

def get_instance():
    global instance_
    if not instance_:
        instance_ = {}
    return instance_


class Singleton:

    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, '_instance'):
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, data):
        self.__data = data


    def do_stuff(self):
        print("Singleton.do_stuff(self={}): data={}".format(self, self.__data))

    @property
    def data(self):
        return self.__data

    @data.setter
    def data(self, new_data):
        self.__data = new_data


class BetterInDiffWay(Singleton):

    def __init__(self, data):
        super().__init__(data)

    def do_stuff(self):
        print("BetterInDiffWay.do_stuff(self={}): data={}".format(self, self._Singleton__data))


def singleton(cls):

    def __new__(cls, *args, **kwargs):
        try:
            return cls._instance
        except AttributeError:
            cls._instance = object.__new__(cls)
            return cls._instance

    cls.__new__ = staticmethod(__new__)
    return cls

@singleton
class OneOnly:

    def __init__(self, data):
        self.data = data


class Borg:

    __shared_state = {}

    def __init__(self, value):
        self.__dict__ = self.__shared_state
        self.state = value

    def do_stuff(self):
        print("Borg.do_stuff(self={}): state={}".format(self, self.state))


def main():
    bs1 = BetterInDiffWay(10)
    bs1.do_stuff()

    s1 = Singleton(1)
    s1.do_stuff()

    s2 = Singleton(2)
    s2.do_stuff()

    s1.do_stuff()

    print("id(s1) == id(s2):", id(s1) == id(s2))

    oo1 = OneOnly(1)
    oo2 = OneOnly(2)

    print("id(oo1) == id(oo2): ", id(oo1) == id(oo2))
    print("oo1.data =", oo1.data)
    print("oo2.data =", oo2.data)

    b1 = Borg(10)
    b1.do_stuff()

    b2 = Borg(20)
    b2.do_stuff()

    b2.state = 13
    b1.do_stuff()

    b1.value = 'Test borga'

    print(b2.value)

    print("*" * 40)

    bs1 = BetterInDiffWay(10)
    bs1.do_stuff()

    bs2 = BetterInDiffWay(20)
    bs2.do_stuff()

if __name__ == '__main__':
    main()