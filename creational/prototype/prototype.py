import copy
import sys

def main():

    object1 = [ 1, 2, ["one", "two"], { 1: "one", 2: "two" }, ("a", 1) ]

    object2 = copy.deepcopy(object1)

    # modify object1
    object1[2].append("three")

    print("object1: ", object1)
    print("object2: ", object2)

class PrototypeManager:

    def __init__(self):
        self.objects = {}

    def register(self, id, obj):
        self.objects[id] = obj

    def unregister(self, id):
        del self.objects[id]

    def clone(self, id, **attr):
        found = self.objects[id]

        if not found:
            raise ValueError("Incorrect object id: {}".format(id))

        obj = copy.deepcopy(found)
        obj.__dict__.update(attr)

        return obj

class Shape:

    def clone(self):
        return copy.deepcopy(self)

class Rectangle(Shape):

    def __init__(self, x, y, w, h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    def draw(self):
        print('Drawing rect at ({}, {}) with w: {} and h {}'.format(self.x, self.y, self.w, self.h))


def main():

    mngr = PrototypeManager()

    r1 = Rectangle(1, 10, 20, 30)
    mngr.register('small-rect', r1)
    mngr.register('large-rect', Rectangle(10, 20, 100, 200))

    cloned_r1 = mngr.clone('small-rect', x = 20, y = 30)
    r1.draw()
    cloned_r1.draw()
    cloned_r2 = cloned_r1.clone()
    cloned_r2.draw()

if __name__ == "__main__":
    main()