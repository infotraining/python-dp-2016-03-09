import random
from collections import namedtuple
from enum import Enum

TreeType = Enum('TreeType', 'pine oak chestnut')
Size = Enum('Size', 'S M L XL')

class TreeFlyweight:

    pool = dict()

    def __new__(cls, tree_type):
        obj = cls.pool.get(tree_type, None)

        if not obj:
            obj = object.__new__(cls)
            cls.pool[tree_type] = obj
            obj.tree_type = tree_type

        return obj

    def render(self, x, y, size):
        print('render a tree of type {} and size {} at ({},{}) with id={}'.format(self.tree_type, size, x, y, id(self)))


def main():

    rnd = random.Random()

    min_point, max_point = 0, 100

    Tree = namedtuple('Tree', 'flyweight, x, y, size')

    forest = []

    for _ in range(30):
        tree_flyweight = TreeFlyweight(TreeType(rnd.randint(1, len(TreeType))))
        size = Size(rnd.randint(1, len(Size)))
        x = rnd.randint(min_point, max_point)
        y = rnd.randint(min_point, max_point)
        forest.append(Tree(tree_flyweight, x, y, size))


    # rendering
    for tree in forest:
        tree.flyweight.render(tree.x, tree.y, tree.size)

    print("#"*40)

    distinct_flyweights = set(tree.flyweight for tree in forest)

    print("distinct trees: {}".format(len(distinct_flyweights)))


if __name__ == '__main__':
    main()


