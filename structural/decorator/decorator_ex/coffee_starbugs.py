import abc


class Coffee(abc.ABC):

    @abc.abstractproperty
    def price(self):
        """returns price of coffee"""

    @abc.abstractproperty
    def description(self):
        """returns description of coffee"""

    @abc.abstractmethod
    def prepare(self):
        """prepares a coffee"""


class BaseCoffee(Coffee):

    def __init__(self, price, description):
        self.__price = price
        self.__description = description

    @property
    def price(self):
        return self.__price

    @property
    def description(self):
        return self.__description


class Espresso(BaseCoffee):

    def __init__(self, price = 4.0):
        super().__init__(price, "Espresso")

    def prepare(self):
        print("Making a perfect espresso: 8g of coffee, 96 Celsius, 16 bar")


class Cappuccino(BaseCoffee):

    def __init__(self, price = 6.0):
        super().__init__(price, "Cappuccino")

    def prepare(self):
        print("Making an espresso combined with a perfect milk foam")


class Latte(BaseCoffee):

    def __init__(self, price = 9.0):
        super().__init__(price, "Latte")

    def prepare(self):
        print("Making a perfect latte")

# CENY DODATKÓW:
# Whipped: 2.5
# Whisky: 10.0
# ExtraEspresso: 4.0


class Decorator(BaseCoffee):

    def __init__(self, coffee, price, description):
        self.__coffee = coffee
        super().__init__(price, description)

    @property
    def price(self):
        return self.__coffee.price + super().price

    @property
    def description(self):
        return self.__coffee.description + " + " + super().price

    def prepare(self):
        self.__coffee.prepare()


class Whipped(Decorator):

    def __init__(self, coffee):
        super().__init__(coffee, 2.5, "Whipped Cream")

    def prepare(self):
        Decorator.prepare(self)
        print("Adding a whipped cream")


class Whisky(Decorator):

    def __init__(self, coffee):
        super().__init__(coffee, 10.0, "Whisky")

    def prepare(self):
        Decorator.prepare(self)
        print("Pouring a 50cl of whisky")


class ExtraEspresso(Decorator):

    espresso = Espresso()

    def __init__(self, coffee):
        super().__init__(coffee, ExtraEspresso.espresso.price, "Extra " + ExtraEspresso.espresso.description)

    def prepare(self):
        Decorator.prepare(self)
        ExtraEspresso.espresso.prepare()


class CoffeeBuilder:

    def __init__(self):
        self.__coffee = None

    def create_base(self, base):

        if not issubclass(base, BaseCoffee):
            raise TypeError("base must be derived from BaseCoffee")

        self.__coffee = base()
        return self

    def add(self, *condiments):

        if not all(issubclass(c, Decorator) for c in condiments):
            raise TypeError("each condiment must be derived from Derived")

        for c in condiments:
            self.__coffee = c(self.__coffee)
        return self

    def get_coffee(self):
        return self.__coffee

def main():

    esp = Espresso()
    whipped = Whipped(esp)
    whisky = Whisky(whipped)
    whisky.prepare()

    print('-' * 40)

    whisky._Decorator__coffee = esp

    print('-' * 40)

    whisky.prepare()

    print('-' * 40)

    cb = CoffeeBuilder()
    coffee = cb.create_base(Espresso).add(Whisky).add(Whipped, ExtraEspresso).get_coffee()
    coffee.prepare()

if __name__ == '__main__':
    main()