import json
import shelve
import urllib.parse
import urllib.request
from datetime import datetime, timedelta


class WeatherProvider:
    def __init__(self):
        self.api_url = "http://api.openweathermap.org/data/2.5/forecast?q={},{}&APPID={}"

    def get_weather_data(self, city, country, api_key):
        city = urllib.parse.quote(city)
        url = self.api_url.format(city, country, api_key)
        return urllib.request.urlopen(url).read()


class Parser:
    def parse_weather_data(self, weather_data):
        parsed = json.loads(weather_data.decode("UTF-8"))
        start_date = None
        result = []

        for data in parsed['list']:
            date = datetime.strptime(data['dt_txt'], '%Y-%m-%d %H:%M:%S')
            start_date = start_date or date
            if start_date.day != date.day:
                return result
            result.append(data['main']['temp'])


class Cache(object):
    def __init__(self, filename):
        self.filename = filename

    def save(self, key, value):
        with shelve.open(self.filename) as shelve_file:
            data = {
                'data': value,
                'expired': datetime.utcnow() + timedelta(hours=3)
            }
            shelve_file[repr(key)] = data

    def load(self, key):
        try:
            with shelve.open(self.filename) as shelve_file:
                result = shelve_file[repr(key)]
                if result['expired'] > datetime.utcnow():
                    return result['data']
        except KeyError:
            pass


class Converter:
    def from_kelvin_to_celcius(self, kelvin):
        return kelvin - 273.15


class Weather:
    def __init__(self, data):
        result = 0

        for r in data:
            result += r

            self.temperature = result / len(data)


class WeatherForcastService:
    def get_forecast(self, city, country):
        cache = Cache('weather')

        cache_result = cache.load(key=(city, country))

        if cache_result:
            return cache_result
        else:
            weather_provider = WeatherProvider()
            weather_data = weather_provider.get_weather_data(city, country, api_key='26e59d9115cd983f4579a60e4cdf28dd')

            parser = Parser()
            parsed_data = parser.parse_weather_data(weather_data)

            weather = Weather(parsed_data)
            converter = Converter()
            temperature_celsius = converter.from_kelvin_to_celcius(weather.temperature)

            cache.save(key=(city, country), value=temperature_celsius)
            return temperature_celsius


if __name__ == '__main__':
    facade = WeatherForcastService()

    forecast = facade.get_forecast('Cracow', 'PL')
    print("Forecast: {}".format(forecast))
