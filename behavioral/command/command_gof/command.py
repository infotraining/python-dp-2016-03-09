import abc
import os
import copy

history = []


class Command(abc.ABC):
    """Command interface"""

    @abc.abstractmethod
    def execute(self):
        """Method to execute the command"""
        pass

    @abc.abstractmethod
    def undo(self):
        """Method to undo the command"""
        pass

    def clone(self):
        return copy.copy(self)


class LsCommand(Command):
    """Concrete command that emulates ls unix command behaviour"""

    def __init__(self, receiver, path):
        self.receiver = receiver
        self.path = path

    def execute(self):
        self.receiver.show_dir(self.path)

    def undo(self):
        """Can not undo this command"""
        pass


class LsReceiver:
    """The receiver knows how to execute the command."""

    def show_dir(self, path):

        filenames = []

        for filename in os.listdir(path):
            if os.path.isfile(os.path.join(path, filename)):
                filenames.append(filename)

        print('Content of dir:', ' '.join(filenames))


class TouchCommand(Command):
    """Concrete command that emulates touch unix command behaviour"""

    def __init__(self, receiver, filename, history):
        self.history = history
        self.receiver = receiver
        self.filename = filename

    def execute(self):
        self.history.append(self.clone())
        self.receiver.create_file(self.filename)

    def undo(self):
        self.receiver.delete_file(self.filename)


class TouchReceiver(object):

    def create_file(self, filename):
        """Actual implementation of unix touch command."""
        with open(filename, 'a'):
            os.utime(filename, None)

    def delete_file(self, filename):
        """Undo unix touch command. Here we simply delete the file."""
        os.remove(filename)


class RmCommand(Command):

    def __init__(self, receiver, filename, history):
        self.receiver = receiver
        self.filename = filename
        self.history = history
        self.backup_name = ''

    def execute(self):
        self.backup_name = '~' + self.filename
        self.history.append(self.clone())
        self.receiver.rename_file(self.filename, self.backup_name)

    def undo(self):
        self.receiver.rename_file(self.backup_name, self.filename)


class RmReceiver:

    def rename_file(self, filename, new_filename):
        """Deletes file with creating backup to restore it in undo method."""
        os.rename(filename, new_filename)


class Button:
    """Invoker class"""

    def __init__(self, name, command):
        self.name = name
        self.__text = ''
        self.command = command

    def on_click(self):
        self.command.execute()


class KeyboardShortcut:
    """Invoker class"""

    def __init__(self, command):
        self.command = command

    def keypress(self):
        self.command.execute()


def main():

    ls_receiver = LsReceiver()
    touch_receiver = TouchReceiver()
    rm_receiver = RmReceiver()

    ls_command = LsCommand(ls_receiver, '.')
    touch_command = TouchCommand(touch_receiver, 'test_file.txt', history)
    rm_command = RmCommand(rm_receiver, 'test_file.txt', history)

    btn_ls = Button("ls", ls_command)
    shrtcut_touch = KeyboardShortcut(touch_command)
    btn_rm = Button("rename", rm_command)

    # executing commands
    btn_ls.on_click()
    shrtcut_touch.keypress();
    btn_ls.on_click()
    btn_rm.on_click()
    btn_ls.on_click()

    print("Undo " + '*' * 40)

    #undo commands

    for cmd in reversed(history):
        cmd.undo()

    btn_ls.on_click()


if __name__ == '__main__':
    main()