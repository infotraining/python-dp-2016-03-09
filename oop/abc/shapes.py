import abc
from collections import namedtuple


Coord = namedtuple('Coord', 'x,y')


class Shape(abc.ABC):

    def __init__(self, x, y):
        self.__coord = Coord(x, y)

    @abc.abstractproperty
    def coordinates(self):
        return self.__coord

    @abc.abstractmethod
    def move(self, dx, dy):
        self.__coord = Coord(self.__coord.x + dx, self.__coord.y + dy)

    @abc.abstractmethod
    def draw(self):
        """Abstract method - must be overriden in subclass"""


@Shape.register
class Circle:

    def __init__(self, x, y, radius):
        self.__x = x
        self.__y = y
        self.__radius = radius

    @property
    def radius(self):
        return self.__radius

    @radius.setter
    def radius(self, new_radius):
        self.__radius = new_radius

    @property
    def coordinates(self):
        return Coord(self.__x, self.__y)

    def move(self, dx, dy):
        self.__x += dx
        self.__y += dy

    def draw(self):
        print("Drawing a circle at {} with radius {}".format(self.coordinates, self.radius))


class Rectangle(Shape):

    def __init__(self, x, y, width, height):
        super().__init__(x, y)
        self.__width = width
        self.__height = height

    @property
    def width(self):
        return self.__width

    @width.setter
    def width(self, new_width):
        self.__width = new_width

    @property
    def height(self):
        return self.__height

    @height.setter
    def height(self, new_height):
        self.__height = new_height

    @property
    def coordinates(self):
        return super().coordinates

    def move(self, dx, dy):
        super().move(dx, dy)

    def draw(self):
        print("Drawing a rectangle at {} with width {} and height {}"
                .format(self.coordinates, self.width, self.height))



def main():
    c = Circle(10, 20, 15)
    c.draw()
    c.move(1, 2)
    c.draw()

    r = Rectangle(0, 15, 10, 20)
    r.draw()
    r.move(10, 10)
    r.draw()


if __name__ == '__main__':
    main()